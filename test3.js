const type1 = {}
const type2 = {}
let w 

type1.star1 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j <= i; ++j)
            w += '*'
        w += '\n'
    }
    return w
}
type1.star2 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = i; j < m; ++j) 
            w += '*'
        w += '\n'
    }
    return w
}
type1.star3 = m => {
    let i, j
    for (i = 0; i < m * 2; ++i) {
        for (j = 0; j <= i%m; ++j)
            w += '*' 
        w += '\n'
    }
    return w
}
type1.star4 = m => {
    let i, j
    for (i = 0; i < m * 2; ++i) {
        for (j = 0; j < Math.abs((i-i%m) * 2 - i); ++j)
            w += '*'
        w += '\n'
    }
    return w
}
type2.star1 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m; ++j)
            w += j <= i ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star2 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m; ++j)
            w += j >= i ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star3 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m; ++j)
            w += j < m - i ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star4 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m; ++j)
            w += j >= m - 1 - i ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star5 = m => {
    let i, j
    for (i = 0; i < m * 2; ++i) {
        for (j = 0; j < m; ++j)
            w += j <= i%m ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star6 = m => {
    let i, j
    for (i = 0; i < m * 2; ++i) {
        for (j = 0; j < m; ++j)
            w += j >= i%m ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star7 = m => {
    let i, j
    for (i = 0; i < m * 2; ++i) {
        for (j = 0; j < m; ++j)
            w += j < m - i%m ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star8 = m => {
    let i, j
    for (i = 0; i < m * 2; ++i) {
        for (j = 0; j < m; ++j)
            w += j >= m - 1 - i%m ? '*' : '-'
        w += '\n'
    }
    return w
}
type2.star9 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m * 2 - 1; ++j)
            j >= m * 2 - 2 - i && (w += '*') || 
            j <= i && (w += '*') || 
            (w += '-')
        w += '\n'
    }
    return w
}
type2.star10 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m * 2 - 1; ++j)
            j >= i && j < m * 2 - 1 - i && 
            (w += '*') || (w += '-')
        w += '\n'
    }
    return w
}
type2.star11 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m * 2 - 1; ++j)
            j >= m - 1 + i && (w += '*') || 
            j < m - i && (w += '*') || 
            (w += '-')
        w += '\n'
    }
    return w
}
type2.star12 = m => {
    let i, j
    for (i = 0; i < m; ++i) {
        for (j = 0; j < m * 2 - 1; ++j)
            j >= m - 1 - i && j < m * 2 - (m - i) &&
            (w += '*') || (w += '-')
        w += '\n'
    }
    return w
}

for (let k in type1) {
    w = ''
    console.log('TYPE1', k)
    console.log(type1[k](5))
}
for (let k in type2) {
    w = ''
    console.log('TYPE2', k)
    console.log(type2[k](5))
}